import {useContext, useState, useEffect} from "react";
import {Table, Button} from "react-bootstrap"

import UserContext from "../UserContext";
import {Navigate, Link} from "react-router-dom";
import Swal from "sweetalert2";

export default function Dashboard (){

	const {user} = useContext(UserContext);

	const [allProducts, setAllProducts] = useState([]);

	const fetchData = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/plants/products`, 
		{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data =>{
			

			setAllProducts(data.map(plant =>{
				return(
					<tr key={plant._id}>
						<td>{plant._id}</td>
						<td>{plant.kind}</td>
						<td>{plant.variety}</td>
						<td>{plant.description}</td>
						<td>{plant.price}</td>
						<td>{plant.stocks}</td>
						<td>{plant.isAvailable ? "Available" : "Out of Stocks"}</td>
						<td>
							{
							(plant.isAvailable)
							?
								<Button variant="danger" size="sm" onClick ={() => archive(plant._id, plant.kind)}>Archive</Button>

							:
							<>
							<Button variant="success" size="sm" onClick ={() => unArchive(plant._id, plant.kind)}>Unarchive</Button>
							<Button as={Link} to={`/editProducts/${plant._id}`} variant="secondary" size="sm">Edit</Button>
							</>

						}
						</td>
					</tr>

					)
			}))
		})


	}
	//Archive
	const archive = (plantId, plantKind) => {
		console.log(plantId);
		console.log(plantKind);

		fetch(`${process.env.REACT_APP_API_URL}/plants/products/${plantId}/archive`, {
			method: "PATCH",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isAvailable: false
			})
		})
		.then(res => res.json())
		.then(data =>{
			

			if(data){
				Swal.fire({
					title: "Product Archived!",
					icon: "success",
					text: `${plantKind} is now out of Stocks.`
				})
			fetchData();
			}
			else{
				Swal.fire({
					title: "Archived Failed!",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				})
			}

		})
	}

	//UnArchive
	const unArchive = (plantId, plantKind) => {
		

		fetch(`${process.env.REACT_APP_API_URL}/plants/products/${plantId}/archive`, {
			method: "PATCH",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				isAvailable: true
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Product Unarchived!",
					icon: "success",
					text: `${plantKind} is now available.`
				})

				fetchData();
			}
			else{
				Swal.fire({
					title: "Unarchived Failed!",
					icon: "error",
					text: "Something went wrong. Please try again later!"
				})
			}

		})
	}


	useEffect(()=>{
		fetchData();
	}, [])

	return(
		(user.isAdmin)

		?
	<>

		<div className="mt-5 mb-3 text-center">
			<h1>Admin Dashboard</h1>
			<Button as={Link} to="/addPlants" variant="primary" size="lg" className="mx-2">Add Products</Button>
			<Button as={Link} to="/users" variant="success" size="lg" className="mx-2">Check Users</Button>
			<Button as={Link} to="/getAllOrders" variant="secondary" size="lg" className="mx-2">Check Orders</Button>

		</div>
		<Table id="dash" striped bordered hover>
		     <thead>
		       <tr >
		         <th>Plant ID</th>
		         <th>Name</th>
		         <th>Variety</th>
		         <th>Description</th>
		         <th>Price</th>
		         <th>Stocks</th>
		         <th>Status</th>
		         <th>Action</th>
		       </tr>
		     </thead>
		     <tbody>
		       {allProducts}
		     </tbody>
		  </Table>
		
	</>
		:
		<Navigate to="/products" />

	)
}