import{useState, useEffect, useContext} from "react";
import {Container, Card, Button, Row, Col} from "react-bootstrap";
import {useParams, useNavigate, Link} from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function ProductView (){

	// Check login User
	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	const {plantId} = useParams();



	const [kind, setKind] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [stocks, setStocks] = useState(0)

	const [quantity, setQuantity] = useState(0);
	const [total, setTotal] = useState(0);
	const [isAvailable, setIsAvailable] = useState(false);

	const purchase = (plantId) =>{
		fetch(`${process.env.REACT_APP_API_URL}/customers/addToCart`,{
			method: "POST",
			headers:{
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				plantId: plantId,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data === true){
				 Swal.fire({
                    title: 'Added to cart!',
                    icon: 'success',
                    toast: true,
                    position: 'center',
                    showConfirmButton: false,
                    timer: 2000,
                    timerProgressBar: false,
                    didOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer)
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
				})
				navigate("/products")
			}
			else{
				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
					text: "Please try again!"

				})

			}
		})
	}

	useEffect(()=>{
		console.log(plantId);
		fetch(`${process.env.REACT_APP_API_URL}/plants/${plantId}`)
		.then(res => res.json())
		.then(data => {

			console.log(data);
			
			setKind(data.kind);
			setDescription(data.description);
			setPrice(data.price);
			setStocks(data.stocks);

		})
	}, [plantId])

	useEffect(()=>{
		setTotal(quantity*price)
	},[quantity])

	useEffect(()=>{
		console.log(quantity)
	    if((quantity>0 && stocks> 0) && (quantity<=stocks)){
	      setIsAvailable(true);
	    }
	    else{
	      setIsAvailable(false);
	    }
	  },[quantity, stocks])

	
	



	return(

		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card>
						<Card.Body className="text-center">
							<Card.Title>You are about to purchase:</Card.Title>
							<Card.Subtitle>{kind}</Card.Subtitle>
						<br/>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Stocks:</Card.Subtitle>
							<Card.Text>{stocks}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text>
							<div className="d-grid gap-2">
							{
								(user.id !== null)
								?
						<Container>
						<Row >
								<Col className="col-3">
									<p>Quantity:</p>
									<input className="w-50" type="number" value={quantity} onChange={e=>{setQuantity(e.target.value); console.log(quantity); console.log(isAvailable)}} />
											</Col>
											<Col className="col-3">
												<p>Total:</p>
												<p>{total}</p>
											</Col>
						{
						(isAvailable)
								?
								<Col>
								<Button variant="success" size="lg" onClick={()=> purchase(plantId)} >Purchase</Button>
								</Col>
								:

								<Col>
								<Button variant="secondary" size="lg" onClick={()=> purchase(plantId)} disabled>Purchase</Button>
								</Col>
							}
								</Row>
						</Container>
								:
								<Button as={Link} to="/login" variant="secondary" size="lg" >Login to Continue</Button>
								
							}

							
							</div>
						
						</Card.Body>		
					</Card>
				</Col>
			</Row>
		</Container>


	)
}