import {Form, Button} from "react-bootstrap";
import {useState, useEffect, useContext} from "react";
import {Navigate} from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function Login (){

		const {user, setUser} = useContext(UserContext);

		const [email, setEmail] = useState("");
		const [password, setPassword] = useState("");


		const [isActive, setIsActive] = useState(false);

	function login(e){
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/customers/login`, {
			method: "POST",
			headers: {
				"Content-Type":"application/json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);
			console.log(data.access)

			if(typeof data.access !== "undefined"){
				localStorage.setItem("token", data.access);
				retrieveUserDetails(data.access);

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to The Garden!"
				});
			}
			else{
				Swal.fire({
					title: "Authetication Failed",
					icon: "error",
					text: "Check your login details and try again."
				});
			}

		})

		// localStorage.setItem("email", email);

		// setUser({

		// 	email: localStorage.getItem("email")
		// })

		setEmail("");
		setPassword("");	
	}


// Retrieve User Details
	const retrieveUserDetails = (token) =>{
		fetch(`${process.env.REACT_APP_API_URL}/customers/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}


	useEffect (() =>{

		if(email !== "" && password !== ""){
			setIsActive(true)

		}
		else{
			setIsActive(false)
		}


	},[email, password]);


	return(

		(user.id !== null)
		
		?
		<Navigate to="/products"/>
		
		:
<>	

	<h1 className="my-5 text-center">Login</h1>

	<div id="login">
<Form onSubmit = {(e) => login(e)}>

     <Form.Group className="mb-3" controlid="email"> 
        <Form.Label id="mail" >Email address</Form.Label>
        <Form.Control type="email" placeholder="Enter email"  value={email} onChange={e => setEmail(e.target.value)}/>
     </Form.Group>
      
      <Form.Group className="mb-3" controlid="password"> 
        <Form.Label id="mail">Password</Form.Label>
        <Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)}/>
      </Form.Group>

      {
      	isActive
      		?

             <Button variant="success" type="submit" id="registerbtn">
                    Login</Button>
      
      		:
             <Button variant="primary" type="submit" id="" disabled>
                    Login</Button>
            		}

    </Form>
    
    </div>
</>

	)

}