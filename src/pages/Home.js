import Banner from "../components/Banner";
import ProductCategories from "../components/ProductCategories";

export default function Home (){

	const data = {

		title:"Welcome to the Garden!!!",
		content:"You can choose from our wide variety of products from our collections of plant varieties and other garden needs.But we're here to tell you that the king of online shopping has a ton of great houseplants! Hirt's Gardens, Costa Farms, and others are reputable sellers where you can source sturdy and stylish plants like the Fiddle Leaf Fig, snake plant, Monstera, and more.",
		destination:"/products",
		label: "Get Started!"
	}

	return(


	<>

		< Banner data={data} />
		< ProductCategories />
	
	</>
	)


}