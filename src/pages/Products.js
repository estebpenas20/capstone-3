import {useEffect, useState, useContext} from "react";

import ProductCards from "../components/ProductCards";
import {Navigate} from "react-router-dom";
import UserContext from "../UserContext";


export default function Products(){

	const [plants, setPlants] = useState([]);

	const {user} = useContext(UserContext);


	useEffect(() =>{
		fetch(`${process.env.REACT_APP_API_URL}/plants/activeProducts`)
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			setPlants(data.map(plant =>{
				return(
					<ProductCards key={plant._id} plantProp={plant} />
				)

			}))
		})
	}, [])

	// const products = productData.map(product =>{
	// 	return(
	// 		<ProductCards key={product.id} productProp={product} />

	// 	)
	// })


	return(
		(user.isAdmin)
		?
			<Navigate to="/admin" />

		:

	<>
		<h1 className="text-center my-3">Products</h1>
		{plants}

	</>
	)


}