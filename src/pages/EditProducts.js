import { useState, useEffect, useContext } from 'react';
import { Navigate, Link, useParams, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

import { Form, Button } from 'react-bootstrap';

export default function EditProducts() {

	const {user} = useContext(UserContext);

	//get the courseId to be updated in the URL
	const { plantId } = useParams();

	const navigate = useNavigate();

	// State hooks to store the values of the input fields
	const [kind, setKind] = useState("");
	const [variety, setVariety] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [stocks, setStocks] = useState(0);
	const [image, setImage] = useState("");

    // State to determine whether submit button is enabled or not
    const [isAvailable, setIsAvailable] = useState(false);

	// This function will be trigger upon clicking the Save button, and will save the update in the database
	function editProduct(e) {

		// Prevents page redirection via form submission
	    e.preventDefault();

	    fetch(`${process.env.REACT_APP_API_URL}/plants/${plantId}`, {
	    	method: "PUT",
	    	headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
			    kind: kind,
			    variety: variety,
			    description: description,
			    price: price,
			    stocks: stocks,
			    image: image
			})
	    })
	    .then(res => res.json())
	    .then(data => {
	    	console.log(data);

	    	if(data){
	    		Swal.fire({
	    		    title: "Product succesfully Updated",
	    		    icon: "success",
	    		    text: `${kind} is now updated`
	    		});

	    		navigate("/admin");
	    	}
	    	else{
	    		Swal.fire({
	    		    title: "Error!",
	    		    icon: "error",
	    		    text: `Something went wrong. Please try again later!`
	    		});
	    	}

	    })

	    // Clear input fields
	    setKind("");
		setVariety("");
	    setDescription("");
	    setPrice(0);
	    setStocks(0);
	    setImage("");

	}


	// Submit button validation
	useEffect(() => {

        // Validation to enable submit button when all fields are populated and set a price and slot greater than zero.
        if (kind !== "" && variety !== "" && description !== "" && price > 0 && stocks > 0){
            setIsAvailable(true);
        } else {
            setIsAvailable(false);
        }

    }, [kind, variety, description, price, stocks]);

	//To get the information of the course to be updated.
    useEffect(()=> {

    	console.log(plantId);

    	fetch(`${process.env.REACT_APP_API_URL}/plants/${plantId}`)
    	.then(res => res.json())
    	.then(data => {

    		console.log(data);

    		// Changing the initial state of the following to the information of the course to be edited.
    	    setKind(data.kind);
			setVariety(data.variety);
	    	setDescription(data.description);
	    	setPrice(data.price);
	    	setStocks(data.stocks);

    	});

    }, [plantId]);

    return (
    	user.isAdmin
    	?
			<>
		    	<h1 className="my-5 text-center">Edit Products</h1>
		        <Form onSubmit={(e) => editProduct(e)}>
		        	<Form.Group controlId="kind" className="mb-3">
		                <Form.Label id="mail">Name:</Form.Label>
		                <Form.Control 
			                type="text" 
			                placeholder="Enter Plant Name" 
			                value = {kind}
			                onChange={e => setKind(e.target.value)}
			                required
		                />
		            </Form.Group>

		            	<Form.Group controlId="variety" className="mb-3">
		                <Form.Label id="mail">Variety:</Form.Label>
		                <Form.Control 
			                type="text" 
			                placeholder="Enter Plant Variety" 
			                value = {variety}
			                onChange={e => setVariety(e.target.value)}
			                required
		                />
		            </Form.Group>

		            <Form.Group controlId="description" className="mb-3">
		                <Form.Label id="mail">Description:</Form.Label>
		                <Form.Control
		                	as="textarea"
		                	rows={3}
			                placeholder="Enter Plant Description" 
			                value = {description}
			                onChange={e => setDescription(e.target.value)}
			                required
		                />
		            </Form.Group>

		            <Form.Group controlId="price" className="mb-3">
		                <Form.Label id="mail">Price:</Form.Label>
		                <Form.Control 
			                type="number" 
			                placeholder="Enter Price" 
			                value = {price}
			                onChange={e => setPrice(e.target.value)}
			                required
		                />
		            </Form.Group>

		            <Form.Group controlId="stocks" className="mb-3">
		                <Form.Label id="mail">Stocks</Form.Label>
		                <Form.Control 
			                type="number" 
			                placeholder="Enter Number of Stocks Available" 
			                value = {stocks}
			                onChange={e => setStocks(e.target.value)}
			                required
		                />
		            </Form.Group>

		            {/*<Form.Group controlId="stocks" className="mb-3">
		                <Form.Label>Image</Form.Label>
		                <Form.Control 
			                type="text" 
			                placeholder="Enter Image URL" 
			                value = {image}
			                onChange={e => setImage(e.target.value)}
			                required
		                />
		            </Form.Group>*/}

		            {/* conditionally render submit button based on isActive state */}
	        	    { isAvailable 
	        	    	? 
	        	    	<Button variant="primary" type="submit" id="submitBtn">
	        	    		Update
	        	    	</Button>
	        	        : 
	        	        <Button variant="danger" type="submit" id="submitBtn" disabled>
	        	        	Update
	        	        </Button>
	        	    }
	        	    	<Button className="m-2" as={Link} to="/admin" variant="success" type="submit" id="submitBtn">
	        	    		Cancel
	        	    	</Button>
		        </Form>
	    	</>
    	:
    	    <Navigate to="/products" />
	    	
    )

}
