import {Form, Button } from "react-bootstrap"

import {useState, useEffect, useContext} from "react";

import {Navigate, useNavigate} from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function Register(){

  const navigate = useNavigate();

  const {user} = useContext(UserContext);

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [phoneNumber, setPhoneNumber]= useState("");
	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");


	const [isActive, setIsActive] = useState(false);


  function registerUser(e){
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/customers/checkEmail`, {
        method: "POST",
        headers: {
            "Content-Type":"application/json"
        },
        body: JSON.stringify({

              email: email

        })

    })
    .then(res => res.json())
    .then(data => {
      console.log(data);

      if(data){
                Swal.fire({
                          title: "Duplicate email found",
                          icon: "error",
                          text: "Kindly provide another email to complete the registration."


                })
      }

      else{
          fetch(`${process.env.REACT_APP_API_URL}/customers/register`, {
          method: "POST",
          headers:{
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            firstName: firstName,
            lastName: lastName,
            email: email,
            phoneNumber: phoneNumber,
            password: password1
          })
        })
        .then(res => res.json())
        .then(data => {
          console.log(data);

          if(data){
            //Clear input fields
            setFirstName("");
            setLastName("");
            setPhoneNumber("");
            setEmail("");
            setPassword1("");
            setPassword2("");

            Swal.fire({
              title: "Registration successful",
              icon: "success",
              text: "Welcome to the Garden!!"
            })

            //redirect the user to the login page after registration.
            navigate("/login");
          }
          else{
            Swal.fire({
              title: "Something went wrong",
              icon: "error",
              text: "Please try again."
            })
          }
        })



      }
    })


  }

	
	useEffect(() =>{
		if((firstName !== "" && lastName !== "" && phoneNumber.length === 11 && email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)){
			setIsActive (true)
		}
		else{
			setIsActive (false)
		}


	}, [firstName, lastName, phoneNumber, email, password1, password2])


	return(
    (user.id !== null)
    ?
    <Navigate to ="/login"/>
    :

	<>	

	<h1 className="my-5 text-center">Register</h1>
<Form id="login" onSubmit = {(e) => registerUser(e)} >
		

      <Form.Group className="mb-3"  controlid="firstName">
      	<Form.Label id="mail">First Name</Form.Label>
        <Form.Control id="mail2" type="text" placeholder="First Name" value={firstName} onChange= {e=> setFirstName (e.target.value)} />
      </Form.Group>

      <Form.Group className="mb-3" controlid="lastName">
        <Form.Label id="mail">Last Name</Form.Label>
        <Form.Control id="mail2" type="text" placeholder="Last Name" value={lastName} onChange= {e=> setLastName (e.target.value)}/>
      </Form.Group>

        <Form.Group className="mb-3" controlid="email"> 
        <Form.Label id="mail">Email address</Form.Label>
        <Form.Control id="mail2" type="email" placeholder="Enter email" value={email} onChange= {e=> setEmail (e.target.value)} />
     </Form.Group>

     <Form.Group className="mb-3" controlid="phoneNumber">
        <Form.Label id="mail" >Phone Number</Form.Label>
        <Form.Control id="mail2" type="number" placeholder="Enter Phone No." value={phoneNumber} onChange= {e=> setPhoneNumber (e.target.value)}/>
      </Form.Group>

      
      <Form.Group className="mb-3" controlid="password1"> 
        <Form.Label id="mail">Password</Form.Label>
        <Form.Control id="mail2" type="password" placeholder="Password" value={password1} onChange= {e=> setPassword1 (e.target.value)}/>
      </Form.Group>

     <Form.Group className="mb-3" controlid="password2">
        <Form.Label id="mail" >Verify Password</Form.Label>
        <Form.Control id="mail2" type="password" placeholder="Confirm Password" value={password2} onChange= {e=> setPassword2 (e.target.value)}/>
      </Form.Group>
      
      {
      	isActive
            ?
            <Button variant="success" type="submit" id="registerbtn">
              Register
            </Button>
      		:

            <Button variant="primary" type="submit" id="registerbt" disabled>
              Register
            </Button>
        }

    </Form>
    </>

	)
}