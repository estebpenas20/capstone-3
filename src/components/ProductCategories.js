import {Card, Row, Col} from "react-bootstrap";

import "../App.css";


export default function ProductCategories(){
	return(
<Row className="mt-3 mb-3" >
    <Col xs={12} md={4} >
        <Card className="cardHighlight p-3" border="success">
            <Card.Body id="cardbody">
                <Card.Title>
                    <h2>Choose from Wide Varieties of Ornamentals!</h2>
                </Card.Title >
                <Card.Text id="plant">
                < img src="https://www.michiganbulb.com/images/350/60028.jpg" /> 
                <h5 >What are Ornamental Plants?</h5>
               
                    They are usually grown in the flower garden for the display of their flowers.It is a plant primarily grown for its beauty either for screening,accent, specimen, color or aesthetic reasons. Common ornamental features include leaves, scent, fruit, stem and bark.
                </Card.Text>
             
            </Card.Body>
        </Card>
    </Col>
    <Col xs={12} md={4}>
        <Card className="cardHighlight p-3" border="success">
            <Card.Body>
                <Card.Title>
                    <h2>Get engage on planting crops consumption!</h2>
                </Card.Title>
                <Card.Text id="plant">
               
              < img src="https://www.dobies.co.uk/product_images/SUTOM30849.jpg " />
             <h5 >What are Vegetable Plants?</h5>
                    The head, or flower, vegetables include artichokes, broccoli, and cauliflower. The fruits commonly considered vegetables by virtue of their use include cucumbers, eggplant, okra, sweet corn, squash, peppers, and tomatoes. Seed vegetables are usually legumes.
                </Card.Text>
              
            </Card.Body>
        </Card>
    </Col>
    <Col xs={12} md={4}>
        <Card className="cardHighlight p-3" border="success">
            <Card.Body>
                <Card.Title>
                    <h2>Selections of planting needs and Planting materials</h2>
                </Card.Title>
                <Card.Text id="plant">
               < img src="https://m.media-amazon.com/images/I/61GEx1oqhOL._AC_.jpg" />
               <h5 >What are Planting Materials?</h5>
                    Planting material refers to seeds, seedlings, or stem cuttings. If you fail to select the good planting material or plants for your garden, you cannot expect a better outcome from planting materials. That's why it's important for you to select the best planting material.
                </Card.Text>
              
            </Card.Body>
        </Card>
    </Col>
</Row>


	)
}