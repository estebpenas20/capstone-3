import {Navbar, Nav, Container} from "react-bootstrap";
import {Link} from "react-router-dom";
import UserContext from "../UserContext";
import {useContext} from "react";



export default function AppNavbar() {

  const{user} = useContext(UserContext);

  return (
    <Navbar collapseOnSelect expand="lg" bg="success" variant="dark">
      <Container>
        <Navbar.Brand id="aboutUs" as={Link} to="/">Peñas Garden</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse >
          <Nav className="ms-auto">
            <Nav.Link as={Link} to="/aboutUs">About Us</Nav.Link>


            <Nav.Link as={Link} to="/">Home</Nav.Link>

            {
              (user.isAdmin)
              ?
            <Nav.Link as={Link} to="/admin">Dashboard</Nav.Link>
              :
              <>
            <Nav.Link as={Link} to="/products">Products</Nav.Link>
           {/* <Nav.Link as={Link} to="/addToCart">myCart</Nav.Link>*/}

            
            </>
            }


            {
              (user.id !== null)
              ?
              <>
              
            <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
            </>
              :
              <>
                <Nav.Link as={Link} to="/register">Register</Nav.Link>
               <Nav.Link as={Link} to="/login">Login</Nav.Link>
              </>

            }

            
          </Nav>
          
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

