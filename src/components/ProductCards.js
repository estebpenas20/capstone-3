import { Card, Button, Row, Col} from "react-bootstrap";
import {useEffect, useState} from "react";
import {Link} from "react-router-dom";
import productcards from "../Styling/productcards.css"

import "../App.css";


export default function ProductCards({plantProp}){

	const {_id, kind, variety, description, price, stocks, image} = plantProp;

	// const [stocks, setStocks] = useState(0);
	// const [purchase, setPurchase] = useState(0);
	// const [isAvailable, setIsAvailable] = useState(false);

	// function purchaseProduct(){
	// 	setPurchase(purchase + 1);
	// 	setStocks(stocks -1);
	// }

	// useEffect(() =>{
	// 	if(stocks === 0){
	// 		setIsAvailable(true);
	// 	}

	// }, [stocks]);
	return (


	 

		<Card className="p-3" id="cardimages">
			<Row>
				<Col md={3}>
				<img className="img-fluid p-3" src= "https://image.shutterstock.com/image-photo/collage-beautiful-flowers-plants-garden-600w-372710284.jpg" width="300px" height="300px"/>
				</Col>
				<Col md={9}>

		      <Card.Body>
		        <Card.Title>Garden Products</Card.Title>
		        <Card.Subtitle>Kind: </Card.Subtitle>
				    <Card.Text>{kind}</Card.Text>
				</Card.Body>
				<Card.Body>
				<Card.Subtitle>Variety: </Card.Subtitle>
				    <Card.Text>{variety}</Card.Text>
		      </Card.Body>
			<Card.Body>
		      	<Card.Subtitle>Price: </Card.Subtitle>
				   <Card.Text>{price}</Card.Text>


			</Card.Body>
					</Col>
				</Row>

		      
		     
		      <Button as={Link} to={`/plants/${_id}`} variant="success" >Check Details!</Button>
		    </Card>

	

  );

}

	{/*	<Card className="CardId" className="p-3">
		    <Card.Body>
		        <Card.Title>
		            Garden Products
		        </Card.Title>

		        <Card.Subtitle>Kind: </Card.Subtitle>
		        <Card.Text>
		            {kind}
		        </Card.Text>

		        <Card.Subtitle>Variety: </Card.Subtitle>
		        <Card.Text>
		            {variety}
		        </Card.Text>




		        <Card.Subtitle>Description </Card.Subtitle>
		        <Card.Text>
		            {description}
		        </Card.Text>

		        <Card.Subtitle>Stocks: </Card.Subtitle>
		        <Card.Text>
		            {stocks}
		        </Card.Text>

		        <Card.Subtitle>Price: </Card.Subtitle>
		        <Card.Text>
		            {price}
		        </Card.Text>
		     */}
		        
		      {/*  <Button as={Link} to={`/plants/${_id}`} variant="primary" >Check Details!</Button>*/}
{/*		    </Card.Body>
// 		</Card>
// 	)
// }
*/}