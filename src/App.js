import {useState, useEffect} from "react";
import {UserProvider} from "./UserContext";

import {BrowserRouter as Router, Routes, Route} from "react-router-dom";

import AppNavbar from "./components/AppNavbar";
import Dashboard from "./pages/Dashboard";
import Home from "./pages/Home";
import Error from "./pages/Error";
import Login from "./pages/Login"
import Logout from "./pages/Logout";
import Products from "./pages/Products"
import ProductView from "./pages/ProductView";
import Register from "./pages/Register";
import AboutUs from "./pages/AboutUs";
import AddProducts from "./pages/AddProducts";
import EditProducts from "./pages/EditProducts";
import CheckUsers from "./pages/CheckUsers";
import AddToCart from "./pages/AddToCart";
import AllOrders from "./pages/AllOrders";
import {Container} from "react-bootstrap";

import './App.css';


function App() {

  const [user, setUser] = useState({
    //email: localStorage.getItem("email")- this causes to logout when refresh button is clicked.

    id: null,
    isAdmin: null

  })

  console.log(user);

  const unsetUser = () =>{
    localStorage.clear();
  }


useEffect(() =>{
  console.log(user);
  console.log(localStorage);


}, [user])

//Update User state upon page load
useEffect(()=>{
  fetch(`${process.env.REACT_APP_API_URL}/customers/details`, {
      headers:{
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
  })
  .then(res => res.json())
  .then(data => {
    console.log(data);

      if(typeof data._id !== "undefined"){
      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      })
    }
    else{
      setUser({
        id: null,
        isAdmin: null
      })

    }
      })
    
  
}, [])


  return (

<UserProvider value={{user, setUser, unsetUser}}>
  <Router>
      <AppNavbar/>
  <Container fluid >
    <Routes>
      <Route exact path = "/" element= {<Home/>} />
      <Route exact path = "/admin" element= {<Dashboard />} />
      <Route exact path = "/products" element= {<Products />} />
      <Route exact path = "/plants/:plantId" element= {<ProductView />} />
      <Route exact path = "/register" element= {<Register/>} />
      <Route exact path = "/login" element= {<Login />} />
      <Route exact path = "/logout" element= {<Logout />} />
      <Route exact path = "/aboutUs" element= {<AboutUs />} />
      <Route exact path ="*" element={<Error />} />
      <Route exact path ="/addPlants" element={<AddProducts />} />
      <Route exact path ="/editProducts/:plantId" element={<EditProducts />} />
      <Route exact path = "/users" element= {<CheckUsers />} />
      <Route exact path = "/addToCart" element= {<AddToCart />} />
       <Route exact path = "/getAllOrders" element= {<AllOrders />} />
    </Routes>
  </Container>
  </Router>

</UserProvider>

  );
}

export default App;
